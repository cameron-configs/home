#!/usr/bin/env bash

set -xe

git submodule update --init

HELPERS='vim.git'

for H in $HELPERS; do
    cd $H
    ./deploy.sh
    cd -
done
