# Git repo for some dot file management

This repo can be check out in `~/`.  It ignores everything not tracked in the repo.

Some files are brought in via git tracking and some are built and deployed instead (see nix flake in `.config-helpers/vim.git`).

To build and deploy configs:

    cd .config-helpers
    ./deploy.sh
