. ~/.bashrc-*

alias _docker_1='docker rm -vf $(docker ps -a -q); docker rmi -f $(docker images -a -q)'
alias _docker_2='docker rmi $(docker images -a --filter=dangling=true -q)'
alias _docker_3='docker rm $(docker ps --filter=status=exited --filter=status=created -q)'
alias _docker_clean='_docker_1 _docker_2 _docker_3'

function pkill() {
	kill $(pgrep $1)
}

function repeat() {
	while true; do $@; sleep 1; done
}
export -f repeat

export HOST_XDG_DATA_DIRS=$XDG_DATA_DIRS
function host_bash_comps() {
	export XDG_DATA_DIRS="$XDG_DATA_DIRS:$HOST_XDG_DATA_DIRS"
}
export -f host_bash_comps

alias inix='echo $IN_NIX_SHELL'

if [[ ! -L /bin/bash ]]; then
	sudo ln -s /run/current-system/sw/bin/bash /bin/bash >/dev/null 2>&1 || true
fi

alias nix-stray-roots='nix-store --gc --print-roots | egrep -v "^(/nix/var|/run/\w+-system|\{memory)"'

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/cameron/.conda/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/cameron/.conda/etc/profile.d/conda.sh" ]; then
        . "/home/cameron/.conda/etc/profile.d/conda.sh"
    else
        export PATH="/home/cameron/.conda/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
